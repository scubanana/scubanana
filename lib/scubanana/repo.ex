defmodule Scubanana.Repo do
  use Ecto.Repo,
    otp_app: :scubanana,
    adapter: Ecto.Adapters.Postgres
end
