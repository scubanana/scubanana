defmodule Scubanana.Dives do
  @moduledoc """
  The Sites context.
  """
  import Ecto.Query, warn: false
  alias Scubanana.Repo
  alias Scubanana.Dives.{Site, Location, DiverProfile}

  @doc """
  Create a dive site
  """
  def add_dive_site(attrs) do
    %Site{} |> Site.create_changeset(attrs) |> Repo.insert()
  end

  @doc """
  Create a location
  """
  def create_location(attrs) do
    %Location{} |> Location.create_changeset(attrs) |> Repo.insert()
  end

  @spec put_site_locations(Site.t(), [Location.t()]) :: {:ok, Site.t()} | {:error, Changeset.t()}
  def put_site_locations(site, locations) do
    site |> Repo.preload([:locations]) |> Site.put_locations(locations) |> Repo.update()
  end

  def create_diver_profile(user, attrs \\ %{}) do
    user
    |> Ecto.build_assoc(:diver_profile)
    |> DiverProfile.create_changeset(attrs)
    |> Repo.insert()
  end
end
