defmodule Scubanana.Dives.DiverProfile do
  use Scubanana.Schema
  import Ecto.Changeset
  alias Scubanana.Dives.Location

  schema "diver_profiles" do
    field :name, :string
    field :bio, :string
    belongs_to :user, Scubanana.Accounts.User
    many_to_many :locations, Location, join_through: "diver_profile_locations"
    timestamps()
  end

  def create_changeset(profile, attrs) do
    profile
    |> cast(attrs, [:name, :bio])
  end
end
