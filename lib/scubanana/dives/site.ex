defmodule Scubanana.Dives.Site do
  use Scubanana.Schema
  import Ecto.Changeset
  alias Scubanana.Locations.Location

  schema "sites" do
    field :name, :string
    field :description, :string
    field :longitude, :float
    field :latitude, :float
    many_to_many :locations, Location, join_through: "site_locations"
    timestamps()
  end

  def create_changeset(site, attrs) do
    site
    |> cast(attrs, [:name, :description, :longitude, :latitude])
    |> validate_required([:name])
  end

  def put_locations(site, locations) do
    site |> change() |> put_assoc(:locations, locations)
  end
end
