defmodule Scubanana.Dives.Location do
  use Scubanana.Schema
  import Ecto.Changeset

  schema "locations" do
    field :category, :string
    field :origin, :string
    field :value, :string
    field :longitude, :float
    field :latitude, :float
    timestamps()
  end

  def create_changeset(location, attrs) do
    location
    |> cast(attrs, [:category, :origin, :value, :longitude, :latitude])
    |> validate_required([:category, :origin, :value])
  end
end
