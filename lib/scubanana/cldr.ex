defmodule Scubanana.Cldr do
  use Cldr,
    otp_app: :scubanana,
    gettext: ScubananaWeb.Gettext
end
