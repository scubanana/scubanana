defmodule ScubananaWeb.LayoutView do
  use ScubananaWeb, :view

  def render(_, assigns) do
    ~H"""
    <html lang="en">
      <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        {{ Phoenix.HTML.Tag.csrf_meta_tag() }}
        {{ live_title_tag assigns[:page_title] || "Scubanana", suffix: " · Phoenix Framework" }}
        <link phx-track-static rel="stylesheet" href={{ Routes.static_path(@conn, "/css/app.css") }}>
        <script
          defer
          phx-track-static
          type="text/javascript"
          src={{ Routes.static_path(@conn, "/js/app.js") }}
        />
      </head>
      <body class="leading-normal tracking-normal">
        {{ @inner_content }}
      </body>
    </html>
    """
  end
end
