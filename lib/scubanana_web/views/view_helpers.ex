defmodule ScubananaWeb.ViewHelpers do
  use Phoenix.HTML

  def a(href, text, additional_class \\ "") do
    link(text, to: href, class: "hover:underline" <> additional_class)
  end

  def input(type, id, name, additional_class \\ "") do
  end

  @doc """
  Returns [Remix](https://remixicon.com) icon tag.
  """
  def remix_icon(name, attrs \\ []) do
    icon_class = "ri-#{name}"
    attrs = Keyword.update(attrs, :class, icon_class, fn class -> "#{icon_class} #{class}" end)
    content_tag(:i, "", attrs)
  end
end
