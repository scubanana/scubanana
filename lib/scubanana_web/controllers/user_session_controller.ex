defmodule ScubananaWeb.UserSessionController do
  use ScubananaWeb, :controller

  alias Scubanana.Accounts
  alias ScubananaWeb.UserAuth

  def create(conn, %{"form_data" => user_params}) do
    %{"email_or_username" => email_or_username, "password" => password} = user_params

    if user = Accounts.get_user_by_email_or_username_and_password(email_or_username, password) do
      UserAuth.log_in_user(conn, user, user_params)
    else
      conn
      |> put_flash(:auth_error_message, "Invalid email/username or password")
      |> redirect(to: Routes.auth_path(conn, :signin))
    end
  end

  def delete(conn, _params) do
    conn
    |> put_flash(:info, "Logged out successfully.")
    |> UserAuth.log_out_user()
  end
end
