defmodule ScubananaWeb.UserRegistrationController do
  use ScubananaWeb, :controller

  alias Scubanana.Accounts
  alias Scubanana.Dives
  alias ScubananaWeb.UserAuth

  def create(conn, %{"form_data" => user_params}) do
    with {:ok, user} <- Accounts.register_user(user_params),
         {:ok, _} <- Dives.create_diver_profile(user) do
      {:ok, _} =
        Accounts.deliver_user_confirmation_instructions(
          user,
          &Routes.user_confirmation_url(conn, :confirm, &1)
        )

      conn
      |> put_flash(:info, "User created successfully.")
      |> UserAuth.log_in_user(user)
    else
      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_flash(:auth_error_changeset, changeset)
        |> put_flash(:auth_error_message, "Registration failed")
        |> redirect(to: Routes.auth_path(conn, :register))
    end
  end
end
