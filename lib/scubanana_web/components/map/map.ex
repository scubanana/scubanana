defmodule ScubananaWeb.Live.Components.Map do
  use Surface.LiveComponent

  data mapid, :string, default: "map_" <> Scubanana.Helpers.randomize(3)
  prop class, :string, default: ""
  prop center_lng, :decimal, default: 0.0
  prop center_lat, :decimal, default: 0.0

  def render(assigns) do
    ~H"""
    <div id={{ @mapid }} class={{ @class }} :hook="Mounted"/>
    """
  end
end
