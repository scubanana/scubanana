let Mounted = {
    mounted() {
        const mapid = this.el.id;
        const style = document.createElement("link");
        style.href = "/css/leaflet.css";
        style.rel = "stylesheet";
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(style);

        const js = document.createElement("script");
        js.src = "/js/leaflet.js";
        js.type = "text/javascript";
        js.onload = function () {
            var map = L.map(mapid).setView([21.08, 55.32], 13);
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map);
        };
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(js);
    }
};

export {Mounted};