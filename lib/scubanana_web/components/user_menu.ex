defmodule ScubananaWeb.Components.UserMenu do
  use Surface.LiveComponent
  use ScubananaWeb, :live_helpers
  alias ScubananaWeb.Router.Helpers, as: Routes
  import ScubananaWeb.Gettext

  def render(assigns) do
    ~H"""
    <div x-data="{open: false}">
      <button
          type="button"
          class="max-w-xs bg-white flex items-center text-sm rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          id="user-menu-button"
          aria-haspopup="true"
          aria-label="User menu"
          @click="open = !open"
          @click.away="open = false"
          @keydown.escape.window="open = false">
        <span class="sr-only">gettext("Open user menu")</span>
        <img class="h-8 w-8 rounded-full" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixqx=MZB4UA6oR3&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="">
      </button>
      <div class= "origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none"
          x-show="open" x-cloak
          x-transition:enter="transition ease-out duration-100"
          x-transition:enter-start="transform opacity-0 scale-95"
          x-transition:enter-end="transform opacity-100 scale-100"
          x-transition:leave="transition ease-in duration-75"
          x-transition:leave-start="transform opacity-100 scale-100"
          x-transition:leave-end="transform opacity-0 scale-95"
          role="menu" aria-orientation="vertical" aria-labelledby="user-menu-button" tabindex="-1">
        <!-- Active: "bg-gray-100", Not Active: "" -->
        <a href="#" class="block px-4 py-2 text-sm text-gray-700" role="menuitem" tabindex="-1" id="user-menu-item-0">
          {{ gettext("Preferences") }}
        </a>

        <Surface.Components.Link
          label={{ gettext("Sign out") }}
          to={{ Routes.user_session_path(@socket, :delete) }}
          method={{ :delete }}
          class="block px-4 py-2 text-sm text-gray-700"
          opts={{ role: "menuitem", tabindex: "-1", id: "user-menu-item-1"}} />
      </div>
    </div>
    """
  end
end
