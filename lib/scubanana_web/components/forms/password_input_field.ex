defmodule ScubananaWeb.Components.Forms.PasswordInputField do
  use Surface.Component
  alias ScubananaWeb.Components.Forms.Field
  alias Surface.Components.Form.PasswordInput
  import Phoenix.HTML.Form

  prop label, :string, required: true
  prop name, :atom, required: true
  prop class, :css_class, default: ""

  def render(assigns) do
    ~H"""
    <Field name={{@name}} label={{ @label }} class={{ @class }}>
      <Context
          get={{ Surface.Components.Form, form: form }}
          get={{ Surface.Components.Form.Field, field: field }}>
        <PasswordInput class= {{
          [
            "pr-10 border-red-300 text-red-900 placeholder-red-300 focus:outline-none focus:ring-red-500 focus:border-red-500": Keyword.has_key?(form.errors, field),
            "focus:ring-indigo-500 focus:border-indigo-500 border-gray-300": !Keyword.has_key?(form.errors, field),
            "shadow-sm block w-full sm:text-sm rounded-md": true
          ]
        }} value={{ input_value(form, :password) }}/>
      </Context>
    </Field>
    """
  end
end
