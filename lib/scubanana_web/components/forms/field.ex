defmodule ScubananaWeb.Components.Forms.Field do
  use Surface.Component
  alias Surface.Components.Form.{Field, Label, ErrorTag}

  slot default, required: true
  prop label, :string, required: true
  prop name, :atom, required: true
  prop class, :css_class, default: ""

  def render(assigns) do
    ~H"""
    <Field name={{@name}} class={{ @class }}>
      <Context get={{ Surface.Components.Form, form: form }}
          get={{ Surface.Components.Form.Field, field: field }}>
        <Label class="block text-sm font-medium text-gray-700">{{ @label }}</Label>
        <div class="mt-1 relative rounded-md shadow-sm">
          <slot/>
            <div :if= {{ Keyword.has_key?(form.errors, field) }} class="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
          <!-- Heroicon name: solid/exclamation-circle -->
                <svg class="h-5 w-5 text-red-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clip-rule="evenodd" />
              </svg>
            </div>
        </div>
        <Label class="mt-2 text-sm text-red-600">
          <ErrorTag class="label-text-alt"/>
        </Label>
      </Context>
    </Field>
    """
  end
end
