defmodule ScubananaWeb.Components.Forms.TextInputField do
  use Surface.Component
  alias ScubananaWeb.Components.Forms.Field
  alias Surface.Components.Form.TextInput

  prop label, :string, required: true
  prop name, :atom, required: true

  def render(assigns) do
    ~H"""
    <Field name={{@name}} label={{ @label }}>
      <Context
          get={{ Surface.Components.Form, form: form }}
          get={{ Surface.Components.Form.Field, field: field }}>
        <TextInput class= {{
          [
            "pr-10 border-red-300 text-red-900 placeholder-red-300 focus:outline-none focus:ring-red-500 focus:border-red-500": Keyword.has_key?(form.errors, field),
            "focus:ring-indigo-500 focus:border-indigo-500 border-gray-300": !Keyword.has_key?(form.errors, field),
            "shadow-sm block w-full sm:text-sm rounded-md": true
          ]
        }}/>
      </Context>
    </Field>
    """
  end
end
