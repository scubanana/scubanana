defmodule ScubananaWeb.Components.RegistrationForm do
  defmodule FormData do
    use Ecto.Schema
    import Ecto.Changeset

    @primary_key false
    embedded_schema do
      field :username, :string
      field :email, :string
      field :password, :string
    end

    def changeset(form_data, attrs \\ %{}) do
      form_data
      |> cast(attrs, [:email, :password, :username])
      |> validate_required([:email, :password, :username])
      |> validate_email()
    end

    defp validate_email(changeset) do
      changeset
      |> validate_format(:email, ~r/^[^\s]+@[^\s]+$/, message: "Invalid Email address")
      |> validate_length(:email, max: 160)
    end

    def validate(params) do
      %FormData{}
      |> changeset(params)
      |> Map.put(:action, :insert)
    end
  end

  defmodule(Component) do
    use Surface.LiveComponent

    alias Surface.Components.LiveRedirect
    alias Surface.Components.Form
    alias ScubananaWeb.Components.Forms.{PasswordInputField, TextInputField, EmailInputField}
    alias ScubananaWeb.Router.Helpers, as: Routes

    prop changeset, :changeset, default: FormData.changeset(%FormData{})
    prop class, :css_class
    prop error_message, :string, default: ""
    data trigger_submit, :boolean, default: false

    def render(assigns) do
      ~H"""
        <div class={{ @class }}>
          <div class="mx-auto w-full max-w-sm lg:w-96">
            <div>
              <h2 class="mt-6 text-3xl font-extrabold text-gray-900">
                Register to Scubanana
              </h2>
              <p class="mt-2 text-sm text-gray-600">
                Or
                <LiveRedirect to={{ Routes.auth_path(@socket, :signin) }} class="font-medium text-indigo-600 hover:text-indigo-500"> already have an account</LiveRedirect>
              </p>
            </div>

            <div class="mt-8">
              <div class="mt-6">

                <div :if= {{ @error_message != nil && @error_message != "" }} class="rounded-md bg-red-50 p-4">
                  <div class="flex">
                    <div class="flex-shrink-0">
                      <!-- Heroicon name: solid/x-circle -->
                      <svg class="h-5 w-5 text-red-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                        <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
                      </svg>
                    </div>
                    <div class="ml-3">
                      <h3 class="text-sm font-medium text-red-800">
                        {{ @error_message }}
                      </h3>
                    </div>
                  </div>
                </div>

                <Form
                  for={{ @changeset }}
                  change="change"
                  action={{ Routes.user_registration_path(@socket, :create) }}
                  submit="submit"
                  opts={{ [phx_trigger_action: @trigger_submit] }}
                  class="space-y-6">
                  <TextInputField name={{:username}} label="Username" />
                  <EmailInputField name={{:email}} label="Email address" />
                  <PasswordInputField name={{:password}} label="Password" />

                  <div>
                    <button type="submit" class="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                      Register
                    </button>
                  </div>
                  </Form>
              </div>
            </div>
          </div>
        </div>
      """
    end

    def handle_event("change", %{"form_data" => form_data} = params, socket) do
      changeset = FormData.validate(form_data) |> Map.put(:action, nil)

      {:noreply,
       assign(socket,
         changeset: changeset,
         error_message: ""
       )}
    end

    def handle_event("submit", %{"form_data" => form_data}, socket) do
      changeset = FormData.validate(form_data) |> Map.put(:action, :insert)

      if changeset.valid? do
        {:noreply, assign(socket, trigger_submit: true)}
      else
        {:noreply, assign(socket, changeset: changeset, trigger_submit: false)}
      end
    end
  end
end
