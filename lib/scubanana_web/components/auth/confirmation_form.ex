defmodule ScubananaWeb.Components.ConfirmationForm do
  use Surface.LiveComponent

  alias Surface.Components.LiveRedirect
  alias Surface.Components.Form
  alias ScubananaWeb.Components.Forms.EmailInputField
  alias ScubananaWeb.Router.Helpers, as: Routes
  alias Scubanana.Accounts.User

  data changeset, :changeset, default: User.confirmation_changeset(%User{})
  data add_email_class, :css_class, default: ""
  prop class, :css_class
  prop error_message, :string, default: ""
  data trigger_submit, :boolean, default: false

  def render(assigns) do
    ~H"""
      <div class={{ @class }}>
        <div class="mx-auto w-full max-w-sm lg:w-96">
          <div>
            <h2 class="mt-6 text-3xl font-extrabold text-gray-900">
              Account confirmation
            </h2>
          </div>

          <div class="mt-8">
            <div class="mt-6">

              <div :if= {{ @error_message != nil && @error_message != "" }} class="rounded-md bg-red-50 p-4">
                <div class="flex">
                  <div class="flex-shrink-0">
                    <!-- Heroicon name: solid/x-circle -->
                    <svg class="h-5 w-5 text-red-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                      <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
                    </svg>
                  </div>
                  <div class="ml-3">
                    <h3 class="text-sm font-medium text-red-800">
                      {{ @error_message }}
                    </h3>
                  </div>
                </div>
              </div>

              <Form
                for={{ @changeset }}
                change="change"
                action={{ Routes.user_confirmation_path(@socket, :create) }}
                submit="submit"
                opts={{ [phx_trigger_action: @trigger_submit] }}
                class="space-y-6">
                <EmailInputField name={{:email}} label="Email address" />

                <div>
                  <button type="submit" class="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Confirm
                  </button>
                </div>
                </Form>
            </div>
          </div>
        </div>
      </div>
    """
  end

  def handle_event("change", %{"user" => form_data}, socket) do
    changeset = User.confirmation_changeset(%User{}, form_data)

    {:noreply,
     assign(socket,
       changeset: changeset,
       error_message: ""
     )}
  end

  def handle_event("submit", %{"user" => form_data}, socket) do
    changeset = User.confirmation_changeset(%User{}, form_data) |> Map.put(:action, :insert)

    if changeset.valid? do
      {:noreply, assign(socket, trigger_submit: true)}
    else
      {:noreply, assign(socket, changeset: changeset, trigger_submit: false)}
    end
  end
end
