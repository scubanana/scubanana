defmodule ScubananaWeb.Live.Auth do
  use Surface.LiveView
  use ScubananaWeb, :live_helpers

  alias ScubananaWeb.Live.Components.NavBar
  alias ScubananaWeb.Components.{SigninForm, RegistrationForm, ConfirmationForm}

  def mount(_params, session, socket) do
    {:ok, socket |> assigns_from_session(session)}
  end

  def render(assigns) do
    ~H"""
    <header class="absolute top-0 left-0 right-0 z-20">
      <NavBar current_user={{ @current_user }}/>
    </header>
    <div class="py-48 max-w-7xl mx-auto sm:px-6 lg:px-8">
      <div class="bg-white flex rounded-lg shadow-lg ring-1 ring-gray-900 ring-opacity-5">
        <SigninForm.Component
                  :if={{ @live_action == :signin }}
                  id="login_form"
                  class="flex-1 flex flex-col justify-center py-12 px-4 sm:px-6 lg:flex-none lg:px-20 xl:px-24"
                  error_message={{ live_flash(@flash, :auth_error_message) }}/>
        <RegistrationForm.Component
                  :if={{ @live_action == :register }}
                  id="registration_form"
                  class="flex-1 flex flex-col justify-center py-12 px-4 sm:px-6 lg:flex-none lg:px-20 xl:px-24"
                  error_message={{ live_flash(@flash, :auth_error_message) }}
                  :props = {{ changeset_from_live_patch(@flash) }}/>
        <ConfirmationForm
                  :if={{ @live_action == :confirm }}
                  id="registration_form"
                  class="flex-1 flex flex-col justify-center py-12 px-4 sm:px-6 lg:flex-none lg:px-20 xl:px-24"
                  error_message={{ live_flash(@flash, :auth_error_message) }}/>
        <div class="hidden lg:block relative w-0 flex-1">
          <img :if={{ @live_action == :signin }} class="absolute inset-0 rounded-r h-full w-full object-cover" src="https://images.unsplash.com/photo-1531809112149-c3e8a981f55b?crop=entropy\u0026cs=tinysrgb\u0026fit=max\u0026fm=jpg\u0026ixid=MnwyMzAyMjN8MHwxfGFsbHx8fHx8fHx8fDE2MjA3NTUyMzc\u0026ixlib=rb-1.2.1\u0026q=80\u0026w=1080" alt="">
          <img :if={{ @live_action == :register || @live_action == :confirm }} class="absolute inset-0 rounded-r h-full w-full object-cover" src="https://images.unsplash.com/photo-1561983240-5544b5c07b04?crop=entropy\u0026cs=tinysrgb\u0026fit=max\u0026fm=jpg\u0026ixid=MnwyMzAyMjN8MHwxfGFsbHx8fHx8fHx8fDE2MjA3NTUyMzc\u0026ixlib=rb-1.2.1\u0026q=80\u0026w=1080" alt="">

        </div>
      </div>
    </div>
    """
  end

  defp changeset_from_live_patch(flash) do
    case live_flash(flash, :auth_error_changeset) do
      nil -> []
      changeset -> [changeset: changeset]
    end
  end
end
