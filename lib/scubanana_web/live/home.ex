defmodule ScubananaWeb.Live.Home do
  use Surface.LiveView
  use ScubananaWeb, :live_helpers

  alias ScubananaWeb.Live.Components.NavBar
  alias ScubananaWeb.Live.Components.Map

  def mount(_params, session, socket) do
    {:ok, socket |> assigns_from_session(session)}
  end

  def render(assigns) do
    ~H"""
    <header class="absolute top-0 left-0 right-0 z-20">
      <NavBar current_user={{ @current_user}}/>
    </header>
    <div class="container flex items-center py-16 my-24 md:my-32 h-full w-full">
      <Map id="main_map" center_lng="00.0" center_lat="00.0" class="flex-auto h-full w-full" />
    </div>
    """
  end
end
