defmodule ScubananaWeb.Router do
  use ScubananaWeb, :router

  import ScubananaWeb.UserAuth

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_live_flash)
    plug(:put_root_layout, {ScubananaWeb.LayoutView, :root})
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)

    plug(Cldr.Plug.SetLocale,
      apps: [cldr: Scubanana.Cldr, gettext: ScubananaWeb.Gettext]
    )

    plug(:put_locale_into_session)
    plug(:fetch_current_user)
  end

  @doc false
  def put_locale_into_session(conn, _opts) do
    %Cldr.LanguageTag{cldr_locale_name: locale} = conn.private.cldr_locale
    put_session(conn, "locale", locale)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", ScubananaWeb do
    pipe_through(:browser)
  end

  # Other scopes may use custom stacks.
  # scope "/api", ScubananaWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through(:browser)
      live_dashboard("/dashboard", metrics: ScubananaWeb.Telemetry, ecto_repos: [Scubanana.Repo])
    end
  end

  ## Authentication routes

  scope "/", ScubananaWeb do
    pipe_through([:browser, :redirect_if_user_is_authenticated])

    live("/users/sign_in", Live.Auth, :signin)
    live("/users/register", Live.Auth, :register)

    post("/users/register", UserRegistrationController, :create)
    post("/users/sign_in", UserSessionController, :create)
    get("/users/reset_password", UserResetPasswordController, :new)
    post("/users/reset_password", UserResetPasswordController, :create)
    get("/users/reset_password/:token", UserResetPasswordController, :edit)
    put("/users/reset_password/:token", UserResetPasswordController, :update)
  end

  scope "/", ScubananaWeb do
    pipe_through([:browser, :require_authenticated_user])

    delete("/users/log_out", UserSessionController, :delete)
    live("/users/confirm", Live.Auth, :confirm)
    post("/users/confirm", UserConfirmationController, :create)

    get("/users/settings", UserSettingsController, :edit)
    put("/users/settings", UserSettingsController, :update)
    get("/users/settings/confirm_email/:token", UserSettingsController, :confirm_email)
  end

  scope "/", ScubananaWeb do
    pipe_through([:browser])
    live("/", Live.Home, :index)
  end
end
