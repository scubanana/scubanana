defmodule ScubananaWeb do
  @moduledoc """
  The entrypoint for defining your web interface, such
  as controllers, views, channels and so on.

  This can be used in your application as:

      use ScubananaWeb, :controller
      use ScubananaWeb, :view

  The definitions below will be executed for every view,
  controller, etc, so keep them short and clean, focused
  on imports, uses and aliases.

  Do NOT define functions inside the quoted expressions
  below. Instead, define any helper function in modules
  and import those modules here.
  """

  def controller do
    quote do
      use Phoenix.Controller, namespace: ScubananaWeb

      import Plug.Conn
      import ScubananaWeb.Gettext
      import Phoenix.LiveView.Controller
      alias ScubananaWeb.Router.Helpers, as: Routes
    end
  end

  def view do
    quote do
      use Phoenix.View,
        root: "lib/scubanana_web/templates",
        namespace: ScubananaWeb

      # Import convenience functions from controllers
      import Phoenix.Controller,
        only: [get_flash: 1, get_flash: 2, view_module: 1, view_template: 1]

      # Include shared imports and aliases for views
      unquote(view_helpers())
    end
  end

  def live_view do
    quote do
      use Phoenix.LiveView,
        layout: {ScubananaWeb.LayoutView, "live.html"}

      unquote(view_helpers())
    end
  end

  def live_component do
    quote do
      use Phoenix.LiveComponent

      unquote(view_helpers())
    end
  end

  def router do
    quote do
      use Phoenix.Router

      import Plug.Conn
      import Phoenix.Controller
      import Phoenix.LiveView.Router
    end
  end

  def channel do
    quote do
      use Phoenix.Channel
      import ScubananaWeb.Gettext
    end
  end

  defp view_helpers do
    quote do
      # Use all HTML functionality (forms, tags, etc)
      use Phoenix.HTML

      # Import LiveView helpers (live_render, live_component, live_patch, etc)
      import Phoenix.LiveView.Helpers

      # Import basic rendering functionality (render, render_layout, etc)
      import Phoenix.View

      import ScubananaWeb.ErrorHelpers
      import ScubananaWeb.Gettext

      # Custom helpers
      import ScubananaWeb.ViewHelpers

      alias ScubananaWeb.Router.Helpers, as: Routes

      import Surface
    end
  end

  def live_helpers do
    quote do
      def set_locale(locale) do
        Scubanana.Cldr.put_locale(locale)
        Gettext.put_locale(locale)
      end

      @session_keys ~w(locale user_token)
      @doc """
      Fill a LiveView assigns with defaults coming from session.
      This can be used on mount to initialized current_user and locale settings
      """
      def assigns_from_session(socket, session) do
        defaults = [current_user: nil, locale: nil]

        assigns =
          Enum.filter(session, fn {k, v} -> Enum.member?(@session_keys, k) end)
          |> Enum.map(fn {k, v} ->
            case k do
              "user_token" ->
                [current_user: Scubanana.Accounts.get_user_by_session_token(v)]

              "locale" ->
                set_locale(v)
                [locale: Scubanana.Cldr.get_locale()]

              _ ->
                []
            end
          end)
          |> Enum.reduce(fn x, acc -> Keyword.merge(x, acc) end)

        Phoenix.LiveView.assign(socket, Keyword.merge(defaults, assigns))
      end
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
