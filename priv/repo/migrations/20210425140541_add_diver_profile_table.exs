defmodule Scubanana.Repo.Migrations.AddDiverProfileTable do
  use Ecto.Migration

  def change do
    create table(:diver_profiles, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :bio, :string
      add :user_id, references(:users, type: :binary_id, on_delete: :delete_all), null: false
      timestamps()
    end

    create table(:diver_profile_locations, primary_key: false) do
      add :diver_profile_id, references(:diver_profiles, type: :binary_id), null: false
      add :location_id, references(:locations, type: :binary_id), null: false
    end

    create unique_index(:diver_profile_locations, [:diver_profile_id, :location_id])

  end
end
