defmodule Scubanana.Repo.Migrations.AddSiteLocationTable do
  use Ecto.Migration

  def change do
    create table(:site_locations, primary_key: false) do
      add :site_id, references(:sites, type: :binary_id), null: false
      add :location_id, references(:locations, type: :binary_id), null: false
      timestamps()
    end

    create unique_index(:site_locations, [:site_id, :location_id])

  end
end
