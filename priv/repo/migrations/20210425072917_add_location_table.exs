defmodule Scubanana.Repo.Migrations.AddLocationTable do
  use Ecto.Migration

  def change do
    execute "CREATE EXTENSION IF NOT EXISTS earthdistance CASCADE"

    create table(:locations, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :category, :string, null: false
      add :origin, :string, null: false
      add :value , :string, null: false
      add :longitude, :float
      add :latitude, :float
      timestamps()
    end

    create index(:locations, ["ll_to_earth(latitude, longitude)"], name: "locations_geo_index", using: "GIST")

  end
end
