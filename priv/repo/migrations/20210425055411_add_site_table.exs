defmodule Scubanana.Repo.Migrations.AddSiteTable do
  use Ecto.Migration

  def change do
    execute "CREATE EXTENSION IF NOT EXISTS earthdistance CASCADE"

    create table(:sites, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string, null: false
      add :description, :text
      add :longitude, :float
      add :latitude, :float
      timestamps()
    end

    create index(:sites, ["ll_to_earth(latitude, longitude)"], name: "sites_geo_index", using: "GIST")
  end
end
