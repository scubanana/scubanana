# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :scubanana,
  ecto_repos: [Scubanana.Repo]

# Don't autogenerate primary keys
config :scubanana, Scubanana.Repo,
  migration_primary_key: false,
  migration_timestamps: [type: :timestamptz]

# Configures the endpoint
config :scubanana, ScubananaWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "E9KiV34nGGnSxpf8dnkG0x1+JE8QdHJGnd5TLssVTlMWA0arwB27ilQ8wZ2RvLHR",
  render_errors: [view: ScubananaWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Scubanana.PubSub,
  live_view: [signing_salt: "13DRr2cn"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :scubanana, Scubanana.Cldr,
  default_locale: "en",
  locales: ["fr", "en"],
  add_fallback_locales: false,
  data_dir: "./priv/cldr",
  precompile_number_formats: ["¤¤#,##0.##"],
  providers: [Cldr.Number],
  generate_docs: true,
  force_locale_download: false,
  json_library: Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
